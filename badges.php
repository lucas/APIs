<?php
include("config.php");
if(isset($_GET['type'])) {
  if(file_exists("includes/badges/".$_GET['type'].".php")) {
    include "includes/badges/".$_GET['type'].".php";
  } else {
    header('Content-Type: text/html');
    echo "<h3>Error, type of badge not detected</h3>";
  }
} else {
  header('Content-Type: text/html');
  include "includes/header.php"; ?>
      <h4 class="menu"><a href="index.php">Home</a> - <b><a href="badges.php">Badges</a></b></h4>
    </header>
    <main>
      <h3><a href="#codeberg"><img class="ri size-1 circular" src="https://codeberg.org/img/favicon.svg" alt="Codeberg"></a> <a href="#"></a></h3>
      <div class="section" id="codeberg">
        <h2><img src="imgs/Codeberg-logo_horizontal.svg" alt="Codeberg Logo" width="150px" height="auto"><span class="badge badge-secondary">GET</span></h2>
        <!-- <img src="https://getiton.l64.repl.co/api.php?product=codeberg&type=blue-on-white&text=GET+IT" alt="GetItOnCodeberg" width="150px" height="auto"> -->
        <br>
        <ul>
          <code><?php if($https==true) { echo "https://"; } else { echo "http://"; } ?><?php echo $hostname.$_SERVER['PHP_SELF']; ?><a style="color: blue;">?type=codeberg</a><a style="color: grey;">&subtype=blue-on-white</a><a style="color: orange;">&background_color=000000</a><a style="color: green;">&text=ONLY ON</a></code>
        </ul>
        <br>
        <li style="color: blue;">First indicate the type of badge</li>
        <li style="color: grey;">Later the subtype (blue-on-white/legacy/white-on-black)</li>
        <li style="color: orange;">Now, it's time for the background color: you can put it in hexdecimal (the code must go without the #, for example "#000000" => "000000"), in letters (for example: "black") or in RGB (rgb(number, number, number), for example rgb(230, 230, 230))</li>
        <li style="color: green;">To finish: simply, put a text</li>
        <br><br>
        <br>
        Original Artwork from <a href="https://codeberg.org/Codeberg/Design/wiki/Branding#user-content-logo">https://codeberg.org/Codeberg/Design/wiki/Branding#user-content-logo</a>
        <br>
        Logo material is licensed under <a href="http://creativecommons.org/licenses/by/4.0/">CC-BY</a>
        <br>
        Codeberg and the Codeberg Logo are trademarks of Codeberg e.V
      </div>
      <br>
      <div class="section">
        <h2>F-droid maybe coming soon</h2>
      </div>
    </main>
<?php include "includes/footer.php";
} ?>