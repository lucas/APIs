# [F-Droid](https://f-droid.org)
From [fdroid/artwork](https://gitlab.com/fdroid/artwork)

F-Droid, Get It On F-Droid, the badge Get It On F-Droid and their logos are trademarks of [F-Droid Limited](https://f-droid.org)