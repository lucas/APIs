<?php
if(isset($_GET['subtype'])) {
  if(file_exists("includes/badges/badge-gen/fdroid/".$_GET['subtype'].".php")) {
    if(isset($_GET['background_color'])) {
      if($_GET['background_color']=="") {
        $background_color = "#000000";
      } else {
        $background_color = $_GET['background_color'];
      }
    } else {
      $background_color = "#000000";
    }
    if(isset($_GET['text'])) {
      if($_GET['text']=="") {
        $text = "AVAILABLE ON";
      } else {
        $text = $_GET['text'];
      }
    } else {
      $text = "AVAILABLE ON";
    }
    include "includes/badges/badges-gen/fdroid/".$_GET['subtype'].".php";
  } else {
    header('Content-Type: text/html');
    die("<h3>Error, badge type not detected</h3>");
  }
} else {
  header('Content-Type: text/html');
  die("<h3>Error, badge type not detected</h3>");
}
?>