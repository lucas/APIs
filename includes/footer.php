    <footer>
      <table style="width: 100%">
        <tr>
          <td>
            &copy;2020 <a href="https://lucas.codeberg.page">Lucas</a>
            <br>
            Codeberg and the Codeberg Logo are trademarks of Codeberg e.V
          </td>
          <td style="text-align: right">
            <a href="https://codeberg.org/lucas/APIs"><img src="badges.php?type=codeberg&subtype=blue-on-white&text=SOURCE+CODE+ON" alt="GetItOnCodeberg" width="120px" height="auto"></a>
          </td>
      </table>
    </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  </body>
</html>