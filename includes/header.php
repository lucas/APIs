<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta chardset="utf-8">
    <meta content="text/html" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Lucas' APIs of awesome is a collection of APIs created by me, absucc, in PHP">
    <meta name="keywords" content="API,APIs,SVG,Badges">
    <title>Lucas' APIs of awesome</title>
    <link rel="icon" href="https://avatars2.githubusercontent.com/u/57920717?s=460&u=eb45becf22894dacc830d3f8b278b7bcad10497e&v=4">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <header>
      <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="https://l64.gitlab.io">
            <img style="text-align: center;" src="https://avatars2.githubusercontent.com/u/57920717?s=460&u=eb45becf22894dacc830d3f8b278b7bcad10497e&v=4" width="30" height="30" alt="Codeberg Logo" loading="lazy">
            Lucas' APIs of awesome <span class="badge bg-secondary">Alpha1.2</span>
          </a>
        </div>
      </nav>